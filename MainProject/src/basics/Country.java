package basics;

import java.util.*;

public class Country {

	public static String countryName;
	public static String capitalCity;
	public static int population;
	public static boolean isINEU;
	public static double GDP;
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter Cauntry name");
		Country.countryName = myScanner.nextLine();
		
		System.out.println("Enter Capital");
		Country.capitalCity = myScanner.nextLine();
		
		System.out.println("Enter Population");
		Country.population = Integer.parseInt (myScanner.nextLine());
		
		System.out.println("Is the Country in EU?");
		Country.isINEU = Boolean.parseBoolean(myScanner.nextLine());
		
		System.out.println("Enter GDP:");
		Country.GDP = Double.parseDouble(myScanner.nextLine());
		myScanner.close();
		show();
		
		
	}

	public static void show() {
		System.out.println("Country Name: " + Country.countryName);
		System.out.println("Capital City: " + Country.capitalCity);
		System.out.println("population: " + Country.population);
		System.out.println("Country in EU: " + Country.isINEU);
		System.out.println("GDP: " + Country.GDP);
		
		
	}
}
