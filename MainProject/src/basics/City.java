package basics;

import java.util.*;

public class City {

	public static String cityName;
	public static String cityHymn;
	public static int population;
	public static boolean isInLatvia;
	public static double CityUnemployment;
	
	public static void main(String[] args) {
		
Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter City Name");
		City.cityName = myScanner.nextLine();
	
		System.out.println("Enter Hymn");
		City.cityHymn = myScanner.nextLine();
		
		System.out.println("Enter Population");
		City.population = Integer.parseInt (myScanner.nextLine());
		
		System.out.println("Is the City in Latvia?");
		City.isInLatvia = Boolean.parseBoolean(myScanner.nextLine());
		
		System.out.println("Enter City Unemployment:");
		City.CityUnemployment = Double.parseDouble(myScanner.nextLine());
		myScanner.close();
		show();
		
	}
		
		
	private static void show() {
	System.out.println("City Name: " + City.cityName);
	System.out.println("City Hymn: " + City.cityHymn);
	System.out.println("Population: " + City.population);
	System.out.println("City in Latvia: " + City.isInLatvia);
	System.out.println("City Unemployment: " + City.CityUnemployment);
		
	}

}
